const app = require("express")
const server = app()

// const socket = require("./socket")

server.listen(3000, () => {
  console.log("listening on port 3000")
})

server.use(app.static('./'))

const router = app.Router();
router.get("/", (req, res) => {
  if(req.url === "/") {
    socket.connect()
  }
  res.send("index.html")
})

server.use(router)