const os = require('os')

let totalMem = os.totalmem() / 1024 / 1024 / 1024
let freeMem = os.freemem() / 1024 / 1024 / 1024

let usedMem = totalMem - freeMem

console.log(`Total mem: ${totalMem}GB \nUsed mem: ${usedMem}GB \nFree mem: ${freeMem}GB`)