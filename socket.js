const net = require("net")
let socket;

const msg = "hello from the client...\n"

// function connect() {
//   console.log('here...')
//   socket = net.connect(6666, "localhost")
// }

// connect()

// function onConnect() {
//   socket.on("connect", () => {
//     console.log("connected to ws server")
//   })
// }

// function sendMessage(msg) {
//   socket.write(msg)
// }

socket = net.connect(9999, "localhost")

socket.on("connect", () => {

  socket.on("data", (data) => {
    console.log(data.toString())
  })

  socket.write(Buffer.from(msg), (error) => {
    if(error) console.log(error)
  })

  socket.on("close", (error) => {
    if(error) console.log(error)

    console.log("logging out....")
    socket.write("logout\n")
  })
})

// module.exports = connect;